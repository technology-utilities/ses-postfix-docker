FROM alpine:3.12

RUN apk add --update --no-cache postfix postfix-pcre cyrus-sasl-plain
RUN echo "maillog_file = /dev/stdout" >> /etc/postfix/main.cf

RUN postconf -e "relayhost = [email-smtp.ap-southeast-1.amazonaws.com]:587" \
"smtp_sasl_auth_enable = yes" \
"smtp_sasl_security_options = noanonymous" \
"smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd" \
"smtp_use_tls = yes" \
"smtp_tls_security_level = encrypt" \
"smtp_tls_note_starttls_offer = yes"

RUN postconf -e 'smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt' \
    'mynetworks = 172.17.0.0/16'

# This should be done within the context of a specific system/site

# COPY postfix/sasl_passwd /etc/postfix/sasl_passwd
# RUN postmap hash:/etc/postfix/sasl_passwd && \
#     chmod 600 /etc/postfix/sasl_passwd && \
#     chown root:postfix /etc/postfix/sasl_passwd

CMD ["/usr/sbin/postfix","start-fg"]
